from time import time
from threading import Thread


def count(n):
    """CPU-bound function."""
    while n > 0:
        n -= 1


if __name__ == '__main__':
    count_number = 100000000
    start = time()
    # count(count_number)
    # count(count_number)
    thrd1 = Thread(target=count, args=(count_number,))
    thrd1.start()
    thrd2 = Thread(target=count, args=(count_number,))
    thrd2.start()

    thrd1.join()
    thrd2.join()
    print(time() - start)
