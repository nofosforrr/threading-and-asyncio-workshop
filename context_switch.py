import asyncio
import time


async def msg(text):
    await asyncio.sleep(0.1)
    print(text)


async def slow_operation():
    print('slow operation started')
    await asyncio.sleep(3)
    # time.sleep(3)
    print('slow operation finished')


async def main():
    await msg('first')

    # Now you want to start long_operation, but you don't want to wait it finised:
    # long_operation should be started, but second msg should be printed immediately.
    # Create task to do so:
    task = asyncio.create_task(slow_operation())
    # await slow_operation()
    await msg('second')

    # Now, when you want, you can await task finised:
    await task


async def with_long_op():
    await msg('first')

    # when this coroutine sleeps, event loop switches context to another task
    await slow_operation() 
    
    await msg('second')


async def context_interceptor():
    await msg('I take over context when u wait')


async def main_another():
    task_1 = asyncio.create_task(with_long_op())
    task_2 = asyncio.create_task(context_interceptor())
    await asyncio.gather(task_1, task_2)

if __name__ == '__main__':
    # asyncio.run(main())
    asyncio.run(main_another())
