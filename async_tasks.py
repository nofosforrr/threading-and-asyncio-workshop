import asyncio


async def f():
    return 1 + 2


async def print_nums():
    num = 1
    while True:
        print(num)
        num += 1
        await asyncio.sleep(0.5)


async def print_time():
    tick = 0
    while True:
        if tick % 3 == 0:
            print(f'{tick} ticks have passed')

        tick += 1
        await asyncio.sleep(1)


async def main():
    nums_task = asyncio.create_task(print_nums())
    print_time_task = asyncio.create_task(print_time())
    tasks = asyncio.gather(nums_task, print_time_task)
    await tasks


if __name__ == '__main__':
    # не нужно делать с версии Python 3.6, если не надо переопределять event loop
    # loop = asyncio.get_event_loop()
    # loop.run_until_complete(main())
    # loop.close()
    asyncio.run(main())
