from threading import Thread
from multiprocessing.pool import ThreadPool
from concurrent.futures import ThreadPoolExecutor
import requests
import asyncio
import httpx


def get_request(url):
    """I/O bound function."""
    response = requests.get(url)
    print('Response is: ', response)


def count_hard():
    """CPU bound function."""
    res = 10*10*1000**100000
    print(res)


def synchronous_main(list_of_urls):
    for url in list_of_urls:
        get_request(url)
        
        
def threading_main(list_of_urls, max_workers):
    with ThreadPoolExecutor(max_workers=max_workers) as pool:
        pool.map(get_request, list_of_urls)


async def async_request(url, session):
    async with session.get(url) as response:
        print('Response is: ', await response.text())


async def async_main(list_of_urls):
    async with httpx.AsyncClient() as session:
        await asyncio.gather(*[async_request(url, session) for url in list_of_urls])


if __name__ == '__main__':
    list_of_urls = [
        'https://www.google.com',
        'https://www.yandex.com',
        'https://www.example.com',
    ] * 30
    asyncio.run(async_main(list_of_urls))
    # threading_main(list_of_urls, 4)
    # synchronous_main(list_of_urls)
    # thrd1 = Thread(target=get_request, args=('https://www.google.com',))
    # thrd2 = Thread(target=get_request, args=('https://www.example.com',))
    # thrd1.start()
    # thrd2.start()
    # thrd1.join()
    # thrd2.join()

    # get_request('https://www.example.com')
    # get_request('https://www.google.com')
    # count_hard()
    print('main thread is finished')
